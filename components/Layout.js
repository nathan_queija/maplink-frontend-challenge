import React from 'react';
import Head from 'next/head';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import injectTapEventPlugin from 'react-tap-event-plugin';
import stylesheet from '../styles/global.scss';
import CircularProgress from 'material-ui/CircularProgress';
import {indigo500, indigo700, redA200} from 'material-ui/styles/colors';
import RaisedButton from 'material-ui/RaisedButton';
import Paper from 'material-ui/Paper';
import TextField from 'material-ui/TextField';
import { getPossibleLocations} from 'actions/villain';
import { connect } from 'react-redux';


const muiTheme = getMuiTheme({
  palette: {
    primary1Color: '#000000',
    primary2Color: '#FFED39',
    accent1Color: '#3A486F',
    pickerHeaderColor: indigo500,
  },
  appBar: {
    height: 50,
  },
});

const styles = {
  button: {
    marginTop: 12,
      alignSelf: 'flex-start'
  },
  control: {
   marginRight: 10
  },
};

if (!process.tapEventInjected) {
  injectTapEventPlugin();
  process.tapEventInjected = true;
}

class Layout extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      latitude: '',
      longitude: ''
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleTouchTap = this.handleTouchTap.bind(this);
  }

  handleChange(event){
    this.setState({
      [event.target.id]: event.target.value
    });
  };

  handleTouchTap(){
    this.props.getPossibleLocations(this.state);
  }

  render() {

    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <div>
          {<style dangerouslySetInnerHTML={{ __html: stylesheet }} />}
          <Head>
            <title>{ this.props.title || 'Home' }</title>
          </Head>

          <div className='container'>
            <Paper  zDepth={3} style={{padding: '30px', marginBottom: '10px'}}>
              <h1 id="welcome">Welcome to BatMobile</h1>
              <div style={{display: 'flex', flexWrap: 'wrap', justifyContent: 'center'}}>
                <TextField
                  hintText="Latitude"
                  id="latitude"
                  floatingLabelText="Latitude"
                  onChange={this.handleChange}
                  style={styles.control}
                />
                <TextField
                  hintText="Longitude"
                  id="longitude"
                  floatingLabelText="Longitude"
                  onChange={this.handleChange}
                  style={styles.control}
                />
                {
                  this.props.villain.fetching ?
                    <CircularProgress/>
                    :
                    <RaisedButton
                      label="Get this villain!"
                      secondary={true}
                      style={styles.button}
                      disabled={this.state.latitude === '' || this.state.longitude === ''}
                      onTouchTap={this.handleTouchTap}
                    />
                }

              </div>
            </Paper>
            {this.props.children}

          </div>

        </div>
      </MuiThemeProvider>
    )
  }
}


const mapStateToProps = ({ villain }) => ({
  villain
});

export default connect(mapStateToProps, {getPossibleLocations})(Layout);
