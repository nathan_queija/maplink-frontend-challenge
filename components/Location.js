import React from 'react';
import { connect } from 'react-redux';
import {setPossibleLocation} from 'actions/villain';


class Location extends React.Component{

    constructor(props){
        super(props);

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(){
        this.props.setPossibleLocation(this.props.location);
    }

    render(){
        const {location} = this.props;
        console.log(this.props);
        return(
            <div className={
                this.props.villain.possibleLocation.location &&  this.props.villain.possibleLocation.place === this.props.location.place? 'current-location': 'location'}
                 onClick={this.handleClick}>
                <h3>{location.place}</h3>
                <p><b>Probability:</b> {Math.round(location.probability)} %</p>
            </div>
        );
    }
}

const mapStateToProps = ({ villain }) => ({
    villain
})


export default connect(mapStateToProps, {setPossibleLocation})(Location);