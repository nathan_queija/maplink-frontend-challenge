import Location from './Location';

const LocationList =  ({info}) => {
    return(
        <div style={{display: 'flex', flexWrap: 'wrap', alignItems: 'center'}}>


                <div style={{display: 'flex', flexWrap: 'wrap', justifyContent: 'space-around', maxHeight: 400, overflow: 'scroll'}}>
                    {info.targets.map(target => <Location key={target.place} location={target} />)}
                </div>


        </div>
    )
};

export default LocationList;