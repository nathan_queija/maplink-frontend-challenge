import { compose, withProps, lifecycle } from "recompose";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  DirectionsRenderer,
  Marker
} from "react-google-maps";

import { connect } from 'react-redux';

import { getRandomPositionInsideBounds} from 'utils';
const batPosition = getRandomPositionInsideBounds();
const Map = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places",
    loadingElement: <div style={{ height: `100%` }} >Loading map...</div>,
    containerElement: <div id="map" style={{ height: '500px', minWidth: '100px', width: '100%' }} />,
    mapElement: <div style={{ height: '100%' }} />,
      batPos: batPosition
  }),
  withScriptjs,
  withGoogleMap,
  lifecycle({
    componentDidMount() {
      this.DirectionsService = new google.maps.DirectionsService({suppressMarkers: true});

    },

    componentWillReceiveProps(nextProps){
        if(nextProps.villain.possibleLocation.location){
            this.DirectionsService.route({
                origin: new google.maps.LatLng(batPosition.latitude, batPosition.longitude),
                destination: new google.maps.LatLng(nextProps.villain.possibleLocation.location.lat, nextProps.villain.possibleLocation.location.lng),
                travelMode: google.maps.TravelMode.DRIVING,
            }, (result, status) => {
                if (status === google.maps.DirectionsStatus.OK) {
                    this.setState({
                        directions: result,
                    });
                } else {
                    console.error(`error fetching directions ${result}`);
                }
            });
        }
    }
  }

  )
)(props => {


  const center = {
      lat: props.villain.possibleLocation.location ? props.villain.possibleLocation.location.lat : props.villainPosition.latitude,
      lng: props.villain.possibleLocation.location ? props.villain.possibleLocation.location.lng : props.villainPosition.longitude
  }
  return (
    <GoogleMap
      zoom={15}
      center={new google.maps.LatLng(center.lat, center.lng)}
    >
      {props.villain.info.targets ? getLocations(props.villain.info.targets, props.villain, props.batPos) : ''}
      {props.directions && props.villain.possibleLocation.location && <DirectionsRenderer directions={props.directions} />}
    </GoogleMap>
  )
}

);

const mapStateToProps = ({ villain }) => ({
  villain
});

function getLocations(targets, villain, batPos){

    const targetsWithBat = [...targets];
    targetsWithBat.push({
        bat: true,
        location: {
            lat: batPos.latitude,
            lng: batPos.longitude
        }
    });

    console.log(targetsWithBat);


    return targetsWithBat.map(target => {

        const current = villain.possibleLocation.location &&  villain.possibleLocation.place === target.place ? true :  false;
        return (
            <Marker
                key={target.place}
                position={{lat: target.location.lat, lng: target.location.lng}}
                icon={{
                    url: `/static/marker-${ target.bat ? 'bat' : (current ? 'on' : 'off')}.png`
                }}
            />
        );``

    });
}

export default connect(mapStateToProps, null)(Map);