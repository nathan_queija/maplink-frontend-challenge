
export function getRandomPositionInsideBounds(){
  const min_lat =  40.746422;
  const max_lat =  40.763328;
  const min_lng =  -73.994753;
  const max_lng =  -73.968039;

  return {
    latitude: getRandomInRange(min_lat, max_lat, 6),
    longitude: getRandomInRange(min_lng, max_lng, 6)
  }

}

function getRandomInRange(from, to, fixed) {
  return (Math.random() * (to - from) + from).toFixed(fixed) * 1;
  // .toFixed() returns string, so ' * 1' is a trick to convert to number
}

