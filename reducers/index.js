import { combineReducers } from 'redux';
import villain from './Villain';

export default combineReducers({
  villain
});
