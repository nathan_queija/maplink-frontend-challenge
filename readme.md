## BatMobile | Maplink Front-end Challenge ##

Hi!
To run this program you need to have Node (>=8) and NPM installed on your local machine.

 1. Install all the dependencies


    `npm install`

 2. Run the program in development mode


    `npm run dev`

 3. Open your browser to start the application
	 `http://localhost:3000`


In this application you can help Batman to find Joker and make Gotham safe again.
You need to inform the coordinates of the villain to receive all the possible location of Joker's next attack.
Make sure that this coordinates are within Gotham boundaries.
You can click on the location to view the route between tour position and the possible location that Joker is.



Stack used in this project
=======

 - React
 - Redux
 - Next.js (for SSR and fast prototyping)
 - Built-in CSS in JS solution
 - SCSS
 - ES6 and some ES7
 - Node
 - Express
 - Flexbox
 - Inline styles