'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _RaisedButton = require('material-ui/RaisedButton');

var _RaisedButton2 = _interopRequireDefault(_RaisedButton);

var _Dialog = require('material-ui/Dialog');

var _Dialog2 = _interopRequireDefault(_Dialog);

var _colors = require('material-ui/styles/colors');

var _FlatButton = require('material-ui/FlatButton');

var _FlatButton2 = _interopRequireDefault(_FlatButton);

var _getMuiTheme = require('material-ui/styles/getMuiTheme');

var _getMuiTheme2 = _interopRequireDefault(_getMuiTheme);

var _MuiThemeProvider = require('material-ui/styles/MuiThemeProvider');

var _MuiThemeProvider2 = _interopRequireDefault(_MuiThemeProvider);

var _reactTapEventPlugin = require('react-tap-event-plugin');

var _reactTapEventPlugin2 = _interopRequireDefault(_reactTapEventPlugin);

var _Map = require('../components/Map');

var _Map2 = _interopRequireDefault(_Map);

var _nextReduxWrapper = require('next-redux-wrapper');

var _nextReduxWrapper2 = _interopRequireDefault(_nextReduxWrapper);

var _store = require('../utils/store');

var _store2 = _interopRequireDefault(_store);

var _villain = require('../actions/villain');

var _Paper = require('material-ui/Paper');

var _Paper2 = _interopRequireDefault(_Paper);

var _LocationList = require('../components/LocationList');

var _LocationList2 = _interopRequireDefault(_LocationList);

var _Layout = require('../components/Layout');

var _Layout2 = _interopRequireDefault(_Layout);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = '/Users/nathanqueija/Desktop/projetos/maplink-frontend-challenge/pages/index.js?entry';


var styles = {
  container: {
    textAlign: 'center',
    paddingTop: 0
  }
};

var muiTheme = {
  palette: {
    accent1Color: _colors.deepOrange500
  }
};

var Index = function (_Component) {
  (0, _inherits3.default)(Index, _Component);

  function Index(props, context) {
    (0, _classCallCheck3.default)(this, Index);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Index.__proto__ || (0, _getPrototypeOf2.default)(Index)).call(this, props, context));

    _this.handleRequestClose = function () {
      _this.setState({
        open: false
      });
    };

    return _this;
  }

  (0, _createClass3.default)(Index, [{
    key: 'getContent',
    value: function getContent() {
      if (this.props.villain.info.targets) {
        return _react2.default.createElement('div', { style: { display: 'flex', flexDirection: 'column', flexWrap: 'wrap', height: 'auto' }, __source: {
            fileName: _jsxFileName,
            lineNumber: 50
          }
        }, _react2.default.createElement(_LocationList2.default, { info: this.props.villain.info, __source: {
            fileName: _jsxFileName,
            lineNumber: 51
          }
        }), _react2.default.createElement(_Map2.default, { villainPosition: this.props.villain.villainPosition, __source: {
            fileName: _jsxFileName,
            lineNumber: 52
          }
        }));
      }

      return _react2.default.createElement('h2', {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 57
        }
      }, 'Hey, Bat! We\'re listening. Just send us the villain coordinates.');
    }
  }, {
    key: 'render',
    value: function render() {

      return _react2.default.createElement(_Layout2.default, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 63
        }
      }, _react2.default.createElement('div', { style: styles.container, __source: {
          fileName: _jsxFileName,
          lineNumber: 64
        }
      }, _react2.default.createElement(_Paper2.default, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 65
        }
      }, this.getContent())));
    }
  }]);

  return Index;
}(_react.Component);

var mapStateToProps = function mapStateToProps(_ref) {
  var villain = _ref.villain;
  return {
    villain: villain
  };
};

exports.default = (0, _nextReduxWrapper2.default)(_store2.default, mapStateToProps, {
  setVillainLocation: _villain.setVillainLocation
})(Index);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInBhZ2VzL2luZGV4LmpzIl0sIm5hbWVzIjpbIlJlYWN0IiwiQ29tcG9uZW50IiwiUmFpc2VkQnV0dG9uIiwiRGlhbG9nIiwiZGVlcE9yYW5nZTUwMCIsIkZsYXRCdXR0b24iLCJnZXRNdWlUaGVtZSIsIk11aVRoZW1lUHJvdmlkZXIiLCJpbmplY3RUYXBFdmVudFBsdWdpbiIsIk1hcCIsIndpdGhSZWR1eCIsImluaXRTdG9yZSIsInNldFZpbGxhaW5Mb2NhdGlvbiIsIlBhcGVyIiwiTG9jYXRpb25MaXN0IiwiTGF5b3V0Iiwic3R5bGVzIiwiY29udGFpbmVyIiwidGV4dEFsaWduIiwicGFkZGluZ1RvcCIsIm11aVRoZW1lIiwicGFsZXR0ZSIsImFjY2VudDFDb2xvciIsIkluZGV4IiwicHJvcHMiLCJjb250ZXh0IiwiaGFuZGxlUmVxdWVzdENsb3NlIiwic2V0U3RhdGUiLCJvcGVuIiwidmlsbGFpbiIsImluZm8iLCJ0YXJnZXRzIiwiZGlzcGxheSIsImZsZXhEaXJlY3Rpb24iLCJmbGV4V3JhcCIsImhlaWdodCIsInZpbGxhaW5Qb3NpdGlvbiIsImdldENvbnRlbnQiLCJtYXBTdGF0ZVRvUHJvcHMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsQUFBTyxBQUFROzs7O0FBQ2YsQUFBTzs7OztBQUNQLEFBQU87Ozs7QUFDUCxBQUFROztBQUNSLEFBQU87Ozs7QUFDUCxBQUFPOzs7O0FBQ1AsQUFBTzs7OztBQUNQLEFBQU87Ozs7QUFDUCxBQUFPOzs7O0FBQ1AsQUFBTzs7OztBQUNQLEFBQU87Ozs7QUFDUCxBQUFTOztBQUNULEFBQU87Ozs7QUFDUCxBQUFPOzs7O0FBRVAsQUFBTzs7Ozs7Ozs7O0FBSVAsSUFBTTs7ZUFDTyxBQUNFLEFBQ1g7Z0JBSEosQUFBZSxBQUNGLEFBRUc7QUFGSCxBQUNUO0FBRlcsQUFDYjs7QUFNRixJQUFNOztBQUFOLEFBQWlCLEFBQ04sQUFDTztBQURQLEFBQ1A7QUFGYSxBQUNmOztJLEFBS0k7aUNBRUo7O2lCQUFBLEFBQWEsT0FBYixBQUFvQixTQUFTO3dDQUFBOztvSUFBQSxBQUNyQixPQURxQixBQUNkOztVQURjLEFBTTdCLHFCQUFxQixZQUFNLEFBQ3pCO1lBQUEsQUFBSztjQUFMLEFBQWMsQUFDTixBQUVUO0FBSGUsQUFDWjtBQVJ5Qjs7V0FJNUI7Ozs7O2lDQVFXLEFBQ1Y7VUFBRyxLQUFBLEFBQUssTUFBTCxBQUFXLFFBQVgsQUFBbUIsS0FBdEIsQUFBMkIsU0FBUSxBQUNqQzsrQkFDTSxjQUFBLFNBQUssT0FBTyxFQUFDLFNBQUQsQUFBVSxRQUFRLGVBQWxCLEFBQWlDLFVBQVUsVUFBM0MsQUFBcUQsUUFBUSxRQUF6RSxBQUFZLEFBQXFFO3NCQUFqRjt3QkFBQSxBQUNFO0FBREY7U0FBQSxrQkFDRSxBQUFDLHdDQUFjLE1BQU0sS0FBQSxBQUFLLE1BQUwsQUFBVyxRQUFoQyxBQUF3QztzQkFBeEM7d0JBREYsQUFDRSxBQUNBO0FBREE7NEJBQ0EsQUFBQywrQkFBSSxpQkFBaUIsS0FBQSxBQUFLLE1BQUwsQUFBVyxRQUFqQyxBQUF5QztzQkFBekM7d0JBSFIsQUFDTSxBQUVFLEFBR1Q7QUFIUzs7QUFLVjs7NkJBQU8sY0FBQTs7b0JBQUE7c0JBQUE7QUFBQTtBQUFBLE9BQUEsRUFBUCxBQUFPLEFBQ1I7Ozs7NkJBRVMsQUFFUjs7NkJBQ0UsQUFBQzs7b0JBQUQ7c0JBQUEsQUFDRTtBQURGO0FBQUEsT0FBQSxrQkFDRSxjQUFBLFNBQUssT0FBTyxPQUFaLEFBQW1CO29CQUFuQjtzQkFBQSxBQUNFO0FBREY7eUJBQ0UsQUFBQzs7b0JBQUQ7c0JBQUEsQUFDQztBQUREO0FBQUEsY0FITixBQUNFLEFBQ0UsQUFDRSxBQUNDLEFBQUssQUFNYjs7Ozs7QUF2Q2lCLEE7O0FBMENwQixJQUFNLGtCQUFrQixTQUFsQixBQUFrQixzQkFBQTtNQUFBLEFBQUcsZUFBSCxBQUFHOzthQUFILEFBQWtCO0FBQUEsQUFDeEM7QUFERixBQUtBOzttRUFBZSxBQUFxQjtBQUFyQixBQUFzQztBQUFBLEFBQ25ELENBRGEsQUFBVSxFQUF6QixBQUFlLEFBRVoiLCJmaWxlIjoiaW5kZXguanM/ZW50cnkiLCJzb3VyY2VSb290IjoiL1VzZXJzL25hdGhhbnF1ZWlqYS9EZXNrdG9wL3Byb2pldG9zL21hcGxpbmstZnJvbnRlbmQtY2hhbGxlbmdlIn0=