"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getRandomPositionInsideBounds = getRandomPositionInsideBounds;
function getRandomPositionInsideBounds() {
  var min_lat = 40.746422;
  var max_lat = 40.763328;
  var min_lng = -73.994753;
  var max_lng = -73.968039;

  return {
    latitude: getRandomInRange(min_lat, max_lat, 6),
    longitude: getRandomInRange(min_lng, max_lng, 6)
  };
}

function getRandomInRange(from, to, fixed) {
  return (Math.random() * (to - from) + from).toFixed(fixed) * 1;
  // .toFixed() returns string, so ' * 1' is a trick to convert to number
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInV0aWxzL2luZGV4LmpzIl0sIm5hbWVzIjpbImdldFJhbmRvbVBvc2l0aW9uSW5zaWRlQm91bmRzIiwibWluX2xhdCIsIm1heF9sYXQiLCJtaW5fbG5nIiwibWF4X2xuZyIsImxhdGl0dWRlIiwiZ2V0UmFuZG9tSW5SYW5nZSIsImxvbmdpdHVkZSIsImZyb20iLCJ0byIsImZpeGVkIiwiTWF0aCIsInJhbmRvbSIsInRvRml4ZWQiXSwibWFwcGluZ3MiOiJBQUNBOzs7OztRQUFPLEFBQVM7QUFBVCx5Q0FBd0MsQUFDN0M7TUFBTSxVQUFOLEFBQWlCLEFBQ2pCO01BQU0sVUFBTixBQUFpQixBQUNqQjtNQUFNLFVBQVcsQ0FBakIsQUFBa0IsQUFDbEI7TUFBTSxVQUFXLENBQWpCLEFBQWtCLEFBRWxCOzs7Y0FDWSxpQkFBQSxBQUFpQixTQUFqQixBQUEwQixTQUQvQixBQUNLLEFBQW1DLEFBQzdDO2VBQVcsaUJBQUEsQUFBaUIsU0FBakIsQUFBMEIsU0FGdkMsQUFBTyxBQUVNLEFBQW1DLEFBR2pEO0FBTFEsQUFDTDs7O0FBTUosU0FBQSxBQUFTLGlCQUFULEFBQTBCLE1BQTFCLEFBQWdDLElBQWhDLEFBQW9DLE9BQU8sQUFDekM7U0FBTyxDQUFDLEtBQUEsQUFBSyxZQUFZLEtBQWpCLEFBQXNCLFFBQXZCLEFBQStCLE1BQS9CLEFBQXFDLFFBQXJDLEFBQTZDLFNBQXBELEFBQTZELEFBQzdEO0FBQ0QiLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL25hdGhhbnF1ZWlqYS9EZXNrdG9wL3Byb2pldG9zL21hcGxpbmstZnJvbnRlbmQtY2hhbGxlbmdlIn0=