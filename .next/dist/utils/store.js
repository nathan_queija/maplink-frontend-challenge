'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _redux = require('redux');

var _reduxThunk = require('redux-thunk');

var _reduxThunk2 = _interopRequireDefault(_reduxThunk);

var _reducers = require('../reducers');

var _reducers2 = _interopRequireDefault(_reducers);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var isClient = typeof window !== 'undefined';

var enhancers = (0, _redux.compose)(typeof window !== 'undefined' && process.env.NODE_ENV !== 'production' ? window.devToolsExtension && window.devToolsExtension() : function (f) {
  return f;
});

var createStoreWithMiddleware = (0, _redux.applyMiddleware)(_reduxThunk2.default)(_redux.createStore);

exports.default = function (initialState) {
  return createStoreWithMiddleware(_reducers2.default, initialState, enhancers);
};
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInV0aWxzL3N0b3JlLmpzIl0sIm5hbWVzIjpbImNyZWF0ZVN0b3JlIiwiY29tcG9zZSIsImFwcGx5TWlkZGxld2FyZSIsInRodW5rIiwicmVkdWNlcnMiLCJpc0NsaWVudCIsIndpbmRvdyIsImVuaGFuY2VycyIsInByb2Nlc3MiLCJlbnYiLCJOT0RFX0VOViIsImRldlRvb2xzRXh0ZW5zaW9uIiwiZiIsImNyZWF0ZVN0b3JlV2l0aE1pZGRsZXdhcmUiLCJpbml0aWFsU3RhdGUiXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLEFBQVMsQUFBYSxBQUFTOztBQUMvQixBQUFPOzs7O0FBQ1AsQUFBTzs7Ozs7O0FBRVAsSUFBTSxXQUFXLE9BQUEsQUFBTyxXQUF4QixBQUFtQzs7QUFFbkMsSUFBTSxnQ0FDSixPQUFBLEFBQU8sV0FBUCxBQUFrQixlQUFlLFFBQUEsQUFBUSxJQUFSLEFBQVksYUFBN0MsQUFBMEQsZUFDdEQsT0FBQSxBQUFPLHFCQUFxQixPQURoQyxBQUNnQyxBQUFPLHNCQUNuQyxhQUFBO1NBQUEsQUFBSztBQUhYLEFBQWtCLENBQUE7O0FBTWxCLElBQU0sNEJBQU4sQUFBa0MsQUFBZ0IsQUFBTyxBQUV6RDs7a0JBQWUsd0JBQUE7U0FBZ0IsQUFBMEIsOENBQTFCLEFBQW9DLGNBQXBELEFBQWdCLEFBQWtEO0FBQWpGIiwiZmlsZSI6InN0b3JlLmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9uYXRoYW5xdWVpamEvRGVza3RvcC9wcm9qZXRvcy9tYXBsaW5rLWZyb250ZW5kLWNoYWxsZW5nZSJ9