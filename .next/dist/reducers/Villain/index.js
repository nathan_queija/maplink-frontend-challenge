'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

exports.default = function () {
  var state = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {
    fetching: false,
    info: {},
    villainPosition: {},
    possibleLocation: {}
  };
  var action = arguments[1];

  switch (action.type) {
    case _actions.VILLAIN_SET_POSSIBLE_LOCATION_ATTACKS:
      return (0, _extends3.default)({}, state, { villainPosition: (0, _extends3.default)({}, action.payload) });
    case _actions.VILLAIN_SET_LOCATION:
      return (0, _extends3.default)({}, state, { info: (0, _extends3.default)({}, action.payload) });
    case _actions.VILLAIN_FETCHING:
      return (0, _extends3.default)({}, state, { fetching: action.payload });
    case _actions.VILLAIN_SET_POSSIBLE_LOCATION:
      return (0, _extends3.default)({}, state, { possibleLocation: action.payload });
    default:
      return state;
  }
};

var _extends2 = require('babel-runtime/helpers/extends');

var _extends3 = _interopRequireDefault(_extends2);

var _actions = require('../../actions');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlZHVjZXJzL1ZpbGxhaW4vaW5kZXguanMiXSwibmFtZXMiOlsiVklMTEFJTl9TRVRfTE9DQVRJT04iLCJWSUxMQUlOX0ZFVENISU5HIiwiVklMTEFJTl9TRVRfUE9TU0lCTEVfTE9DQVRJT05fQVRUQUNLUyIsIlZJTExBSU5fU0VUX1BPU1NJQkxFX0xPQ0FUSU9OIiwic3RhdGUiLCJmZXRjaGluZyIsImluZm8iLCJ2aWxsYWluUG9zaXRpb24iLCJwb3NzaWJsZUxvY2F0aW9uIiwiYWN0aW9uIiwidHlwZSIsInBheWxvYWQiXSwibWFwcGluZ3MiOiI7Ozs7OztrQkFFZSxZQUtKO01BTGMsQUFLZDtjQUxzQixBQUNyQixBQUNWO1VBRitCLEFBRXpCLEFBQ047cUJBSCtCLEFBR2QsQUFDZjtzQkFKNkIsQUFJWCxBQUNYO0FBTHNCLEFBQy9CO01BSUMsQUFBUSxtQkFDVDs7VUFBUSxPQUFSLEFBQWUsQUFDYjtBQUFBLEFBQUssQUFDSDt3Q0FBQSxBQUFZLFNBQU8sNENBQXFCLE9BQXhDLEFBQW1CLEFBQTRCLEFBQ2pEO0FBQUEsQUFBSyxBQUNIO3dDQUFBLEFBQVksU0FBTyxpQ0FBVSxPQUE3QixBQUFtQixBQUFpQixBQUN0QztBQUFBLEFBQUssQUFDSDt3Q0FBQSxBQUFZLFNBQU8sVUFBVSxPQUE3QixBQUFvQyxBQUNwQztBQUFBLEFBQUssQUFDRDt3Q0FBQSxBQUFZLFNBQU8sa0JBQWtCLE9BQXJDLEFBQTRDLEFBQ2xEO0FBQ0U7YUFWSixBQVVJLEFBQU8sQUFFWjs7Ozs7Ozs7QUFwQkQsQUFBUyxBQUFzQixBQUFrQixBQUF1QyxBQUV4RiIsImZpbGUiOiJpbmRleC5qcyIsInNvdXJjZVJvb3QiOiIvVXNlcnMvbmF0aGFucXVlaWphL0Rlc2t0b3AvcHJvamV0b3MvbWFwbGluay1mcm9udGVuZC1jaGFsbGVuZ2UifQ==