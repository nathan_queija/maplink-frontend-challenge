'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _redux = require('redux');

var _Villain = require('./Villain');

var _Villain2 = _interopRequireDefault(_Villain);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = (0, _redux.combineReducers)({
  villain: _Villain2.default
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInJlZHVjZXJzL2luZGV4LmpzIl0sIm5hbWVzIjpbImNvbWJpbmVSZWR1Y2VycyIsInZpbGxhaW4iXSwibWFwcGluZ3MiOiI7Ozs7OztBQUFBLEFBQVM7O0FBQ1QsQUFBTyxBQUVQOzs7Ozs7O0FBQUEsQUFBZSxBQUFnQjtBQUFBLEFBQzdCLENBRGEiLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL25hdGhhbnF1ZWlqYS9EZXNrdG9wL3Byb2pldG9zL21hcGxpbmstZnJvbnRlbmQtY2hhbGxlbmdlIn0=