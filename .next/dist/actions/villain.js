'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.getPossibleLocations = getPossibleLocations;
exports.setPossibleLocation = setPossibleLocation;

var _regenerator = require('babel-runtime/regenerator');

var _regenerator2 = _interopRequireDefault(_regenerator);

var _asyncToGenerator2 = require('babel-runtime/helpers/asyncToGenerator');

var _asyncToGenerator3 = _interopRequireDefault(_asyncToGenerator2);

var _isomorphicUnfetch = require('isomorphic-unfetch');

var _isomorphicUnfetch2 = _interopRequireDefault(_isomorphicUnfetch);

var _ = require('./');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function getPossibleLocations(location) {
  var _this = this;

  return function () {
    var _ref = (0, _asyncToGenerator3.default)( /*#__PURE__*/_regenerator2.default.mark(function _callee(dispatch) {
      var response, json;
      return _regenerator2.default.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              dispatch({ type: _.VILLAIN_FETCHING, payload: true });
              dispatch({ type: _.VILLAIN_SET_POSSIBLE_LOCATION_ATTACKS, payload: location });
              _context.next = 4;
              return (0, _isomorphicUnfetch2.default)('http://code-challenge.maplink.com.br/coordinate?q=' + location.latitude + ',' + location.longitude);

            case 4:
              response = _context.sent;

              console.log(response);

              if (!(response.status !== 200)) {
                _context.next = 10;
                break;
              }

              alert('Hey, Bat! Are you sure you are sending us the correct information?');
              dispatch({ type: _.VILLAIN_FETCHING, payload: false });
              return _context.abrupt('return');

            case 10:
              _context.next = 12;
              return response.json();

            case 12:
              json = _context.sent;

              console.log(json);

              dispatch({ type: _.VILLAIN_SET_LOCATION, payload: json });
              dispatch({ type: _.VILLAIN_FETCHING, payload: false });

            case 16:
            case 'end':
              return _context.stop();
          }
        }
      }, _callee, _this);
    }));

    return function (_x) {
      return _ref.apply(this, arguments);
    };
  }();
}

function setPossibleLocation(location) {
  return function (dispatch) {
    dispatch({ type: _.VILLAIN_SET_POSSIBLE_LOCATION, payload: location });
  };
}
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImFjdGlvbnMvdmlsbGFpbi5qcyJdLCJuYW1lcyI6WyJmZXRjaCIsIlZJTExBSU5fU0VUX0xPQ0FUSU9OIiwiVklMTEFJTl9GRVRDSElORyIsIlZJTExBSU5fU0VUX1BPU1NJQkxFX0xPQ0FUSU9OX0FUVEFDS1MiLCJWSUxMQUlOX1NFVF9QT1NTSUJMRV9MT0NBVElPTiIsImdldFBvc3NpYmxlTG9jYXRpb25zIiwibG9jYXRpb24iLCJkaXNwYXRjaCIsInR5cGUiLCJwYXlsb2FkIiwibGF0aXR1ZGUiLCJsb25naXR1ZGUiLCJyZXNwb25zZSIsImNvbnNvbGUiLCJsb2ciLCJzdGF0dXMiLCJhbGVydCIsImpzb24iLCJzZXRQb3NzaWJsZUxvY2F0aW9uIl0sIm1hcHBpbmdzIjoiOzs7OztRQVFPLEFBQVM7UUFvQlQsQUFBUzs7Ozs7Ozs7OztBQTVCaEIsQUFBTzs7OztBQUNQLEFBQ0UsQUFDQSxBQUNBLEFBQ0UsQUFHSjs7OztBQUFPLDhCQUFBLEFBQThCLFVBQVM7Y0FDNUM7O3FCQUFBO3dGQUFPLGlCQUFBLEFBQU0sVUFBTjtvQkFBQTtvRUFBQTtrQkFBQTsyQ0FBQTtpQkFDTDt1QkFBUyxFQUFBLEFBQUUsQUFBTSwwQkFBa0IsU0FBbkMsQUFBUyxBQUFtQyxBQUM1Qzt1QkFBUyxFQUFBLEFBQUUsQUFBTSwrQ0FBdUMsU0FGbkQsQUFFTCxBQUFTLEFBQXdEOzhCQUY1RDtxQkFHa0Isd0ZBQTJELFNBQTNELEFBQW9FLGlCQUFZLFNBSGxHLEFBR2tCLEFBQXlGOztpQkFBMUc7QUFIRCxrQ0FJTDs7c0JBQUEsQUFBUSxJQUpILEFBSUwsQUFBWTs7b0JBRVQsU0FBQSxBQUFTLFdBTlAsQUFNa0IsTUFObEI7Z0NBQUE7QUFBQTtBQU9IOztvQkFBQSxBQUFNLEFBQ0o7dUJBQVMsRUFBQSxBQUFFLEFBQU0sMEJBQWtCLFNBUmxDLEFBUUQsQUFBUyxBQUFtQztxQ0FSM0M7O2lCQUFBOzhCQUFBO3FCQVdjLFNBWGQsQUFXYyxBQUFTOztpQkFBdEI7QUFYRCw4QkFZTDs7c0JBQUEsQUFBUSxJQUFSLEFBQVksQUFFWjs7dUJBQVMsRUFBQSxBQUFFLEFBQU0sOEJBQXNCLFNBQXZDLEFBQVMsQUFBdUMsQUFDaEQ7dUJBQVMsRUFBQSxBQUFFLEFBQU0sMEJBQWtCLFNBZjlCLEFBZUwsQUFBUyxBQUFtQzs7aUJBZnZDO2lCQUFBOzhCQUFBOztBQUFBO2tCQUFBO0FBQVA7O3lCQUFBOzhCQUFBO0FBQUE7QUFpQkQ7QUFFRDs7QUFBTyw2QkFBQSxBQUE2QixVQUFTLEFBQ3pDO1NBQU8sb0JBQVksQUFDZjthQUFTLEVBQUEsQUFBRSxBQUFNLHVDQUErQixTQUFoRCxBQUFTLEFBQWdELEFBQzVEO0FBRkQsQUFHSCIsImZpbGUiOiJ2aWxsYWluLmpzIiwic291cmNlUm9vdCI6Ii9Vc2Vycy9uYXRoYW5xdWVpamEvRGVza3RvcC9wcm9qZXRvcy9tYXBsaW5rLWZyb250ZW5kLWNoYWxsZW5nZSJ9