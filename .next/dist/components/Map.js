"use strict";

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _toConsumableArray2 = require("babel-runtime/helpers/toConsumableArray");

var _toConsumableArray3 = _interopRequireDefault(_toConsumableArray2);

var _react = require("react");

var _react2 = _interopRequireDefault(_react);

var _recompose = require("recompose");

var _reactGoogleMaps = require("react-google-maps");

var _reactRedux = require("react-redux");

var _utils = require("../utils");

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = "/Users/nathanqueija/Desktop/projetos/maplink-frontend-challenge/components/Map.js";

var batPosition = (0, _utils.getRandomPositionInsideBounds)();
var Map = (0, _recompose.compose)((0, _recompose.withProps)({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places",
    loadingElement: _react2.default.createElement("div", { style: { height: "100%" }, __source: {
            fileName: _jsxFileName,
            lineNumber: 17
        }
    }, "Loading map..."),
    containerElement: _react2.default.createElement("div", { id: "map", style: { height: '500px', minWidth: '100px', width: '100%' }, __source: {
            fileName: _jsxFileName,
            lineNumber: 18
        }
    }),
    mapElement: _react2.default.createElement("div", { style: { height: '100%' }, __source: {
            fileName: _jsxFileName,
            lineNumber: 19
        }
    }),
    batPos: batPosition
}), _reactGoogleMaps.withScriptjs, _reactGoogleMaps.withGoogleMap, (0, _recompose.lifecycle)({
    componentDidMount: function componentDidMount() {
        this.DirectionsService = new google.maps.DirectionsService({ suppressMarkers: true });
    },
    componentWillReceiveProps: function componentWillReceiveProps(nextProps) {
        var _this = this;

        if (nextProps.villain.possibleLocation.location) {
            this.DirectionsService.route({
                origin: new google.maps.LatLng(batPosition.latitude, batPosition.longitude),
                destination: new google.maps.LatLng(nextProps.villain.possibleLocation.location.lat, nextProps.villain.possibleLocation.location.lng),
                travelMode: google.maps.TravelMode.DRIVING
            }, function (result, status) {
                if (status === google.maps.DirectionsStatus.OK) {
                    _this.setState({
                        directions: result
                    });
                } else {
                    console.error("error fetching directions " + result);
                }
            });
        }
    }
}))(function (props) {

    var center = {
        lat: props.villain.possibleLocation.location ? props.villain.possibleLocation.location.lat : props.villainPosition.latitude,
        lng: props.villain.possibleLocation.location ? props.villain.possibleLocation.location.lng : props.villainPosition.longitude
    };
    return _react2.default.createElement(_reactGoogleMaps.GoogleMap, {
        zoom: 15,
        center: new google.maps.LatLng(center.lat, center.lng),
        __source: {
            fileName: _jsxFileName,
            lineNumber: 58
        }
    }, props.villain.info.targets ? getLocations(props.villain.info.targets, props.villain, props.batPos) : '', props.directions && props.villain.possibleLocation.location && _react2.default.createElement(_reactGoogleMaps.DirectionsRenderer, { directions: props.directions, __source: {
            fileName: _jsxFileName,
            lineNumber: 63
        }
    }));
});

var mapStateToProps = function mapStateToProps(_ref) {
    var villain = _ref.villain;
    return {
        villain: villain
    };
};

function getLocations(targets, villain, batPos) {

    var targetsWithBat = [].concat((0, _toConsumableArray3.default)(targets));
    targetsWithBat.push({
        bat: true,
        location: {
            lat: batPos.latitude,
            lng: batPos.longitude
        }
    });

    console.log(targetsWithBat);

    return targetsWithBat.map(function (target) {

        var current = villain.possibleLocation.location && villain.possibleLocation.place === target.place ? true : false;
        return _react2.default.createElement(_reactGoogleMaps.Marker, {
            key: target.place,
            position: { lat: target.location.lat, lng: target.location.lng },
            icon: {
                url: "/static/marker-" + (target.bat ? 'bat' : current ? 'on' : 'off') + ".png"
            },
            __source: {
                fileName: _jsxFileName,
                lineNumber: 92
            }
        });"";
    });
}

exports.default = (0, _reactRedux.connect)(mapStateToProps, null)(Map);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvTWFwLmpzIl0sIm5hbWVzIjpbImNvbXBvc2UiLCJ3aXRoUHJvcHMiLCJsaWZlY3ljbGUiLCJ3aXRoU2NyaXB0anMiLCJ3aXRoR29vZ2xlTWFwIiwiR29vZ2xlTWFwIiwiRGlyZWN0aW9uc1JlbmRlcmVyIiwiTWFya2VyIiwiY29ubmVjdCIsImdldFJhbmRvbVBvc2l0aW9uSW5zaWRlQm91bmRzIiwiYmF0UG9zaXRpb24iLCJNYXAiLCJnb29nbGVNYXBVUkwiLCJsb2FkaW5nRWxlbWVudCIsImhlaWdodCIsImNvbnRhaW5lckVsZW1lbnQiLCJtaW5XaWR0aCIsIndpZHRoIiwibWFwRWxlbWVudCIsImJhdFBvcyIsImNvbXBvbmVudERpZE1vdW50IiwiRGlyZWN0aW9uc1NlcnZpY2UiLCJnb29nbGUiLCJtYXBzIiwic3VwcHJlc3NNYXJrZXJzIiwiY29tcG9uZW50V2lsbFJlY2VpdmVQcm9wcyIsIm5leHRQcm9wcyIsInZpbGxhaW4iLCJwb3NzaWJsZUxvY2F0aW9uIiwibG9jYXRpb24iLCJyb3V0ZSIsIm9yaWdpbiIsIkxhdExuZyIsImxhdGl0dWRlIiwibG9uZ2l0dWRlIiwiZGVzdGluYXRpb24iLCJsYXQiLCJsbmciLCJ0cmF2ZWxNb2RlIiwiVHJhdmVsTW9kZSIsIkRSSVZJTkciLCJyZXN1bHQiLCJzdGF0dXMiLCJEaXJlY3Rpb25zU3RhdHVzIiwiT0siLCJzZXRTdGF0ZSIsImRpcmVjdGlvbnMiLCJjb25zb2xlIiwiZXJyb3IiLCJjZW50ZXIiLCJwcm9wcyIsInZpbGxhaW5Qb3NpdGlvbiIsImluZm8iLCJ0YXJnZXRzIiwiZ2V0TG9jYXRpb25zIiwibWFwU3RhdGVUb1Byb3BzIiwidGFyZ2V0c1dpdGhCYXQiLCJwdXNoIiwiYmF0IiwibG9nIiwibWFwIiwiY3VycmVudCIsInBsYWNlIiwidGFyZ2V0IiwidXJsIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7OztBQUFBLEFBQVMsQUFBUyxBQUFXOztBQUM3QixBQUNFLEFBQ0EsQUFDQSxBQUNBLEFBQ0E7O0FBR0YsQUFBUzs7QUFFVCxBQUFTOzs7Ozs7QUFDVCxJQUFNLGNBQU4sQUFBb0I7QUFDcEIsSUFBTTtrQkFDTSxBQUNNLEFBQ2Q7b0NBQWdCLGNBQUEsU0FBSyxPQUFPLEVBQUUsUUFBZCxBQUFZO3NCQUFaO3dCQUFBO0FBQUE7S0FBQSxFQUZSLEFBRVEsQUFDaEI7NkRBQXVCLElBQUwsQUFBUSxPQUFNLE9BQU8sRUFBRSxRQUFGLEFBQVUsU0FBUyxVQUFuQixBQUE2QixTQUFTLE9BQTNELEFBQXFCLEFBQTZDO3NCQUFsRTt3QkFIVixBQUdVLEFBQ2xCO0FBRGtCO0tBQUE7dURBQ0QsT0FBTyxFQUFFLFFBQWQsQUFBWSxBQUFVO3NCQUF0Qjt3QkFKSixBQUlJLEFBQ1Y7QUFEVTtLQUFBO1lBTEosQUFDVixBQUFVLEFBS0UsQUFFWixBQUNBO0FBUlUsQUFDUixDQURGO0FBU1Usb0RBQ1ksQUFDbEI7YUFBQSxBQUFLLG9CQUFvQixJQUFJLE9BQUEsQUFBTyxLQUFYLEFBQWdCLGtCQUFrQixFQUFDLGlCQUE1RCxBQUF5QixBQUFrQyxBQUFrQixBQUU5RTtBQUpPLEFBTVI7QUFOUSxrRUFBQSxBQU1rQixXQUFVO29CQUNoQzs7WUFBRyxVQUFBLEFBQVUsUUFBVixBQUFrQixpQkFBckIsQUFBc0MsVUFBUyxBQUMzQztpQkFBQSxBQUFLLGtCQUFMLEFBQXVCO3dCQUNYLElBQUksT0FBQSxBQUFPLEtBQVgsQUFBZ0IsT0FBTyxZQUF2QixBQUFtQyxVQUFVLFlBRDVCLEFBQ2pCLEFBQXlELEFBQ2pFOzZCQUFhLElBQUksT0FBQSxBQUFPLEtBQVgsQUFBZ0IsT0FBTyxVQUFBLEFBQVUsUUFBVixBQUFrQixpQkFBbEIsQUFBbUMsU0FBMUQsQUFBbUUsS0FBSyxVQUFBLEFBQVUsUUFBVixBQUFrQixpQkFBbEIsQUFBbUMsU0FGL0YsQUFFWixBQUFvSCxBQUNqSTs0QkFBWSxPQUFBLEFBQU8sS0FBUCxBQUFZLFdBSDVCLEFBQTZCLEFBR1U7QUFIVixBQUN6QixlQUdELFVBQUEsQUFBQyxRQUFELEFBQVMsUUFBVyxBQUNuQjtvQkFBSSxXQUFXLE9BQUEsQUFBTyxLQUFQLEFBQVksaUJBQTNCLEFBQTRDLElBQUksQUFDNUM7MEJBQUEsQUFBSztvQ0FBTCxBQUFjLEFBQ0UsQUFFbkI7QUFIaUIsQUFDVjtBQUZSLHVCQUlPLEFBQ0g7NEJBQUEsQUFBUSxxQ0FBUixBQUEyQyxBQUM5QztBQUNKO0FBWkQsQUFhSDtBQUNKO0FBaENPLEFBVVYsQUFBVTtBQUFBLEFBQ1IsQ0FERixHQTBCQSxpQkFBUyxBQUdUOztRQUFNO2FBQ0csTUFBQSxBQUFNLFFBQU4sQUFBYyxpQkFBZCxBQUErQixXQUFXLE1BQUEsQUFBTSxRQUFOLEFBQWMsaUJBQWQsQUFBK0IsU0FBekUsQUFBa0YsTUFBTSxNQUFBLEFBQU0sZ0JBRHhGLEFBQ3dHLEFBQ25IO2FBQUssTUFBQSxBQUFNLFFBQU4sQUFBYyxpQkFBZCxBQUErQixXQUFXLE1BQUEsQUFBTSxRQUFOLEFBQWMsaUJBQWQsQUFBK0IsU0FBekUsQUFBa0YsTUFBTSxNQUFBLEFBQU0sZ0JBRnZHLEFBQWUsQUFFd0csQUFFdkg7QUFKZSxBQUNYOzJCQUlGLEFBQUM7Y0FBRCxBQUNRLEFBQ047Z0JBQVEsSUFBSSxPQUFBLEFBQU8sS0FBWCxBQUFnQixPQUFPLE9BQXZCLEFBQThCLEtBQUssT0FGN0MsQUFFVSxBQUEwQzs7c0JBRnBEO3dCQUFBLEFBSUc7QUFKSDtBQUNFLEtBREYsUUFJRyxBQUFNLFFBQU4sQUFBYyxLQUFkLEFBQW1CLFVBQVUsYUFBYSxNQUFBLEFBQU0sUUFBTixBQUFjLEtBQTNCLEFBQWdDLFNBQVMsTUFBekMsQUFBK0MsU0FBUyxNQUFyRixBQUE2QixBQUE4RCxVQUo5RixBQUl3RyxBQUNyRyxVQUFBLEFBQU0sY0FBYyxNQUFBLEFBQU0sUUFBTixBQUFjLGlCQUFsQyxBQUFtRCw0QkFBWSxBQUFDLHFEQUFtQixZQUFZLE1BQWhDLEFBQXNDO3NCQUF0Qzt3QkFOcEUsQUFDRSxBQUtrRSxBQUdyRTtBQUhxRTtLQUFBO0FBakR0RSxBQUFZLENBQUE7O0FBd0RaLElBQU0sa0JBQWtCLFNBQWxCLEFBQWtCLHNCQUFBO1FBQUEsQUFBRyxlQUFILEFBQUc7O2lCQUFILEFBQWtCO0FBQUEsQUFDeEM7QUFERjs7QUFJQSxTQUFBLEFBQVMsYUFBVCxBQUFzQixTQUF0QixBQUErQixTQUEvQixBQUF3QyxRQUFPLEFBRTNDOztRQUFNLDREQUFOLEFBQU0sQUFBcUIsQUFDM0I7bUJBQUEsQUFBZTthQUFLLEFBQ1gsQUFDTDs7aUJBQ1MsT0FEQyxBQUNNLEFBQ1o7aUJBQUssT0FKYixBQUFvQixBQUVOLEFBRU0sQUFJcEI7QUFOYyxBQUNOO0FBSFksQUFDaEI7O1lBT0osQUFBUSxJQUFSLEFBQVksQUFHWjs7MEJBQU8sQUFBZSxJQUFJLGtCQUFVLEFBRWhDOztZQUFNLFVBQVUsUUFBQSxBQUFRLGlCQUFSLEFBQXlCLFlBQWEsUUFBQSxBQUFRLGlCQUFSLEFBQXlCLFVBQVUsT0FBekUsQUFBZ0YsUUFBaEYsQUFBd0YsT0FBeEcsQUFBZ0gsQUFDaEg7K0JBQ0ksQUFBQztpQkFDUSxPQURULEFBQ2dCLEFBQ1o7c0JBQVUsRUFBQyxLQUFLLE9BQUEsQUFBTyxTQUFiLEFBQXNCLEtBQUssS0FBSyxPQUFBLEFBQU8sU0FGckQsQUFFYyxBQUFnRCxBQUMxRDs7MENBQzRCLE9BQUEsQUFBTyxNQUFQLEFBQWEsUUFBUyxVQUFBLEFBQVUsT0FBeEQsQUFBK0QsU0FKdkUsQUFHVTtBQUFBLEFBQ0Y7OzBCQUpSOzRCQURKLEFBQ0k7QUFBQTtBQUNJLFNBREosRUFPRixBQUVMO0FBYkQsQUFBTyxBQWNWLEtBZFU7QUFnQlg7O2tCQUFlLHlCQUFBLEFBQVEsaUJBQVIsQUFBeUIsTUFBeEMsQUFBZSxBQUErQiIsImZpbGUiOiJNYXAuanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL25hdGhhbnF1ZWlqYS9EZXNrdG9wL3Byb2pldG9zL21hcGxpbmstZnJvbnRlbmQtY2hhbGxlbmdlIn0=