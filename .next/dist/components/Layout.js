'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _defineProperty2 = require('babel-runtime/helpers/defineProperty');

var _defineProperty3 = _interopRequireDefault(_defineProperty2);

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _head = require('next/dist/lib/head.js');

var _head2 = _interopRequireDefault(_head);

var _getMuiTheme = require('material-ui/styles/getMuiTheme');

var _getMuiTheme2 = _interopRequireDefault(_getMuiTheme);

var _MuiThemeProvider = require('material-ui/styles/MuiThemeProvider');

var _MuiThemeProvider2 = _interopRequireDefault(_MuiThemeProvider);

var _reactTapEventPlugin = require('react-tap-event-plugin');

var _reactTapEventPlugin2 = _interopRequireDefault(_reactTapEventPlugin);

var _global = require('../styles/global.scss');

var _global2 = _interopRequireDefault(_global);

var _CircularProgress = require('material-ui/CircularProgress');

var _CircularProgress2 = _interopRequireDefault(_CircularProgress);

var _colors = require('material-ui/styles/colors');

var _RaisedButton = require('material-ui/RaisedButton');

var _RaisedButton2 = _interopRequireDefault(_RaisedButton);

var _Paper = require('material-ui/Paper');

var _Paper2 = _interopRequireDefault(_Paper);

var _TextField = require('material-ui/TextField');

var _TextField2 = _interopRequireDefault(_TextField);

var _villain = require('../actions/villain');

var _reactRedux = require('react-redux');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = '/Users/nathanqueija/Desktop/projetos/maplink-frontend-challenge/components/Layout.js';


var muiTheme = (0, _getMuiTheme2.default)({
  palette: {
    primary1Color: '#000000',
    primary2Color: '#FFED39',
    accent1Color: '#3A486F',
    pickerHeaderColor: _colors.indigo500
  },
  appBar: {
    height: 50
  }
});

var styles = {
  button: {
    marginTop: 12,
    alignSelf: 'flex-start'
  },
  control: {
    marginRight: 10
  }
};

if (!process.tapEventInjected) {
  (0, _reactTapEventPlugin2.default)();
  process.tapEventInjected = true;
}

var Layout = function (_React$Component) {
  (0, _inherits3.default)(Layout, _React$Component);

  function Layout(props) {
    (0, _classCallCheck3.default)(this, Layout);

    var _this = (0, _possibleConstructorReturn3.default)(this, (Layout.__proto__ || (0, _getPrototypeOf2.default)(Layout)).call(this, props));

    _this.state = {
      latitude: '',
      longitude: ''
    };

    _this.handleChange = _this.handleChange.bind(_this);
    _this.handleTouchTap = _this.handleTouchTap.bind(_this);
    return _this;
  }

  (0, _createClass3.default)(Layout, [{
    key: 'handleChange',
    value: function handleChange(event) {
      this.setState((0, _defineProperty3.default)({}, event.target.id, event.target.value));
    }
  }, {
    key: 'handleTouchTap',
    value: function handleTouchTap() {
      this.props.getPossibleLocations(this.state);
    }
  }, {
    key: 'render',
    value: function render() {

      return _react2.default.createElement(_MuiThemeProvider2.default, { muiTheme: muiTheme, __source: {
          fileName: _jsxFileName,
          lineNumber: 70
        }
      }, _react2.default.createElement('div', {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 71
        }
      }, _react2.default.createElement('style', { dangerouslySetInnerHTML: { __html: _global2.default }, __source: {
          fileName: _jsxFileName,
          lineNumber: 72
        }
      }), _react2.default.createElement(_head2.default, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 73
        }
      }, _react2.default.createElement('title', {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 74
        }
      }, this.props.title || 'Home')), _react2.default.createElement('div', { className: 'container', __source: {
          fileName: _jsxFileName,
          lineNumber: 77
        }
      }, _react2.default.createElement(_Paper2.default, { zDepth: 3, style: { padding: '30px', marginBottom: '10px' }, __source: {
          fileName: _jsxFileName,
          lineNumber: 78
        }
      }, _react2.default.createElement('h1', { id: 'welcome', __source: {
          fileName: _jsxFileName,
          lineNumber: 79
        }
      }, 'Welcome to BatMobile'), _react2.default.createElement('div', { style: { display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }, __source: {
          fileName: _jsxFileName,
          lineNumber: 80
        }
      }, _react2.default.createElement(_TextField2.default, {
        hintText: 'Latitude',
        id: 'latitude',
        floatingLabelText: 'Latitude',
        onChange: this.handleChange,
        style: styles.control,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 81
        }
      }), _react2.default.createElement(_TextField2.default, {
        hintText: 'Longitude',
        id: 'longitude',
        floatingLabelText: 'Longitude',
        onChange: this.handleChange,
        style: styles.control,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 88
        }
      }), this.props.villain.fetching ? _react2.default.createElement(_CircularProgress2.default, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 97
        }
      }) : _react2.default.createElement(_RaisedButton2.default, {
        label: 'Get this villain!',
        secondary: true,
        style: styles.button,
        disabled: this.state.latitude === '' || this.state.longitude === '',
        onTouchTap: this.handleTouchTap,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 99
        }
      }))), this.props.children)));
    }
  }]);

  return Layout;
}(_react2.default.Component);

var mapStateToProps = function mapStateToProps(_ref) {
  var villain = _ref.villain;
  return {
    villain: villain
  };
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, { getPossibleLocations: _villain.getPossibleLocations })(Layout);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvTGF5b3V0LmpzIl0sIm5hbWVzIjpbIlJlYWN0IiwiSGVhZCIsImdldE11aVRoZW1lIiwiTXVpVGhlbWVQcm92aWRlciIsImluamVjdFRhcEV2ZW50UGx1Z2luIiwic3R5bGVzaGVldCIsIkNpcmN1bGFyUHJvZ3Jlc3MiLCJpbmRpZ281MDAiLCJpbmRpZ283MDAiLCJyZWRBMjAwIiwiUmFpc2VkQnV0dG9uIiwiUGFwZXIiLCJUZXh0RmllbGQiLCJnZXRQb3NzaWJsZUxvY2F0aW9ucyIsImNvbm5lY3QiLCJtdWlUaGVtZSIsInBhbGV0dGUiLCJwcmltYXJ5MUNvbG9yIiwicHJpbWFyeTJDb2xvciIsImFjY2VudDFDb2xvciIsInBpY2tlckhlYWRlckNvbG9yIiwiYXBwQmFyIiwiaGVpZ2h0Iiwic3R5bGVzIiwiYnV0dG9uIiwibWFyZ2luVG9wIiwiYWxpZ25TZWxmIiwiY29udHJvbCIsIm1hcmdpblJpZ2h0IiwicHJvY2VzcyIsInRhcEV2ZW50SW5qZWN0ZWQiLCJMYXlvdXQiLCJwcm9wcyIsInN0YXRlIiwibGF0aXR1ZGUiLCJsb25naXR1ZGUiLCJoYW5kbGVDaGFuZ2UiLCJiaW5kIiwiaGFuZGxlVG91Y2hUYXAiLCJldmVudCIsInNldFN0YXRlIiwidGFyZ2V0IiwiaWQiLCJ2YWx1ZSIsIl9faHRtbCIsInRpdGxlIiwicGFkZGluZyIsIm1hcmdpbkJvdHRvbSIsImRpc3BsYXkiLCJmbGV4V3JhcCIsImp1c3RpZnlDb250ZW50IiwidmlsbGFpbiIsImZldGNoaW5nIiwiY2hpbGRyZW4iLCJDb21wb25lbnQiLCJtYXBTdGF0ZVRvUHJvcHMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQUFBLEFBQU87Ozs7QUFDUCxBQUFPOzs7O0FBQ1AsQUFBTzs7OztBQUNQLEFBQU87Ozs7QUFDUCxBQUFPOzs7O0FBQ1AsQUFBTzs7OztBQUNQLEFBQU87Ozs7QUFDUCxBQUFRLEFBQVcsQUFBVzs7QUFDOUIsQUFBTzs7OztBQUNQLEFBQU87Ozs7QUFDUCxBQUFPOzs7O0FBQ1AsQUFBUzs7QUFDVCxBQUFTOzs7Ozs7O0FBR1QsSUFBTTs7bUJBQ0ssQUFDUSxBQUNmO21CQUZPLEFBRVEsQUFDZjtrQkFITyxBQUdPLEFBQ2Q7QUFMeUIsQUFDbEIsQUFJWSxBQUVyQjtBQU5TLEFBQ1A7O1lBRkosQUFBaUIsQUFBWSxBQU9uQixBQUNFO0FBREYsQUFDTjtBQVJ5QixBQUMzQixDQURlOztBQVlqQixJQUFNOztlQUNJLEFBQ0ssQUFDVDtlQUhTLEFBQ0wsQUFFTyxBQUVmO0FBSlEsQUFDTjs7aUJBRkosQUFBZSxBQUtKLEFBQ0s7QUFETCxBQUNSO0FBTlksQUFDYjs7QUFTRixJQUFJLENBQUMsUUFBTCxBQUFhLGtCQUFrQixBQUM3QjtBQUNBO1VBQUEsQUFBUSxtQkFBUixBQUEyQixBQUM1Qjs7O0ksQUFFSztrQ0FFSjs7a0JBQUEsQUFBWSxPQUFPO3dDQUFBOztzSUFBQSxBQUNYLEFBRU47O1VBQUEsQUFBSztnQkFBUSxBQUNELEFBQ1Y7aUJBRkYsQUFBYSxBQUVBLEFBR2I7QUFMYSxBQUNYOztVQUlGLEFBQUssZUFBZSxNQUFBLEFBQUssYUFBTCxBQUFrQixLQUF0QyxBQUNBO1VBQUEsQUFBSyxpQkFBaUIsTUFBQSxBQUFLLGVBQUwsQUFBb0IsS0FUekIsQUFTakI7V0FDRDs7Ozs7aUMsQUFFWSxPQUFNLEFBQ2pCO1dBQUEsQUFBSywyQ0FDRixNQUFBLEFBQU0sT0FEVCxBQUNnQixJQUFLLE1BQUEsQUFBTSxPQUQzQixBQUNrQyxBQUVuQzs7OztxQ0FFZSxBQUNkO1dBQUEsQUFBSyxNQUFMLEFBQVcscUJBQXFCLEtBQWhDLEFBQXFDLEFBQ3RDOzs7OzZCQUVRLEFBRVA7OzZCQUNFLEFBQUMsNENBQWlCLFVBQWxCLEFBQTRCO29CQUE1QjtzQkFBQSxBQUNFO0FBREY7T0FBQSxrQkFDRSxjQUFBOztvQkFBQTtzQkFBQSxBQUNHO0FBREg7QUFBQSxrREFDVSx5QkFBeUIsRUFBaEMsQUFBZ0MsQUFBRSxBQUFRO29CQUExQztzQkFESCxBQUNHLEFBQ0Q7QUFEQzswQkFDRCxBQUFDOztvQkFBRDtzQkFBQSxBQUNFO0FBREY7QUFBQSx5QkFDRSxjQUFBOztvQkFBQTtzQkFBQSxBQUFTO0FBQVQ7QUFBQSxjQUFTLEFBQUssTUFBTCxBQUFXLFNBSHhCLEFBRUUsQUFDRSxBQUE2QixBQUcvQiwwQkFBQSxjQUFBLFNBQUssV0FBTCxBQUFlO29CQUFmO3NCQUFBLEFBQ0U7QUFERjt5QkFDRSxBQUFDLGlDQUFPLFFBQVIsQUFBZ0IsR0FBRyxPQUFPLEVBQUMsU0FBRCxBQUFVLFFBQVEsY0FBNUMsQUFBMEIsQUFBZ0M7b0JBQTFEO3NCQUFBLEFBQ0U7QUFERjt5QkFDRSxjQUFBLFFBQUksSUFBSixBQUFPO29CQUFQO3NCQUFBO0FBQUE7U0FERixBQUNFLEFBQ0EseUNBQUEsY0FBQSxTQUFLLE9BQU8sRUFBQyxTQUFELEFBQVUsUUFBUSxVQUFsQixBQUE0QixRQUFRLGdCQUFoRCxBQUFZLEFBQW9EO29CQUFoRTtzQkFBQSxBQUNFO0FBREY7eUJBQ0UsQUFBQztrQkFBRCxBQUNXLEFBQ1Q7WUFGRixBQUVLLEFBQ0g7MkJBSEYsQUFHb0IsQUFDbEI7a0JBQVUsS0FKWixBQUlpQixBQUNmO2VBQU8sT0FMVCxBQUtnQjs7b0JBTGhCO3NCQURGLEFBQ0UsQUFPQTtBQVBBO0FBQ0UsMEJBTUYsQUFBQztrQkFBRCxBQUNXLEFBQ1Q7WUFGRixBQUVLLEFBQ0g7MkJBSEYsQUFHb0IsQUFDbEI7a0JBQVUsS0FKWixBQUlpQixBQUNmO2VBQU8sT0FMVCxBQUtnQjs7b0JBTGhCO3NCQVJGLEFBUUUsQUFRRTtBQVJGO0FBQ0UsZUFPQSxBQUFLLE1BQUwsQUFBVyxRQUFYLEFBQW1CLDJCQUNqQixBQUFDOztvQkFBRDtzQkFERixBQUNFO0FBQUE7QUFBQSxPQUFBLG9CQUVBLEFBQUM7ZUFBRCxBQUNRLEFBQ047bUJBRkYsQUFFYSxBQUNYO2VBQU8sT0FIVCxBQUdnQixBQUNkO2tCQUFVLEtBQUEsQUFBSyxNQUFMLEFBQVcsYUFBWCxBQUF3QixNQUFNLEtBQUEsQUFBSyxNQUFMLEFBQVcsY0FKckQsQUFJbUUsQUFDakU7b0JBQVksS0FMZCxBQUttQjs7b0JBTG5CO3NCQXRCVixBQUNFLEFBRUUsQUFtQk0sQUFXUDtBQVhPO0FBQ0UsT0FERixVQVdQLEFBQUssTUF6Q2QsQUFDRSxBQUNFLEFBTUUsQUFpQ2MsQUFPckI7Ozs7O0VBMUVrQixnQkFBTSxBOztBQThFM0IsSUFBTSxrQkFBa0IsU0FBbEIsQUFBa0Isc0JBQUE7TUFBQSxBQUFHLGVBQUgsQUFBRzs7YUFBSCxBQUFrQjtBQUFBLEFBQ3hDO0FBREYsQUFJQTs7a0JBQWUseUJBQUEsQUFBUSxpQkFBaUIsRUFBekIsQUFBeUIsQUFBQyx1REFBekMsQUFBZSxBQUFpRCIsImZpbGUiOiJMYXlvdXQuanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL25hdGhhbnF1ZWlqYS9EZXNrdG9wL3Byb2pldG9zL21hcGxpbmstZnJvbnRlbmQtY2hhbGxlbmdlIn0=