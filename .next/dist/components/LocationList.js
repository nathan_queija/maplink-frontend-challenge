'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _Location = require('./Location');

var _Location2 = _interopRequireDefault(_Location);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = '/Users/nathanqueija/Desktop/projetos/maplink-frontend-challenge/components/LocationList.js';


var LocationList = function LocationList(_ref) {
    var info = _ref.info;

    return _react2.default.createElement('div', { style: { display: 'flex', flexWrap: 'wrap', alignItems: 'center' }, __source: {
            fileName: _jsxFileName,
            lineNumber: 5
        }
    }, _react2.default.createElement('div', { style: { display: 'flex', flexWrap: 'wrap', justifyContent: 'space-around', maxHeight: 400, overflow: 'scroll' }, __source: {
            fileName: _jsxFileName,
            lineNumber: 8
        }
    }, info.targets.map(function (target) {
        return _react2.default.createElement(_Location2.default, { key: target.place, location: target, __source: {
                fileName: _jsxFileName,
                lineNumber: 9
            }
        });
    })));
};

exports.default = LocationList;
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvTG9jYXRpb25MaXN0LmpzIl0sIm5hbWVzIjpbIkxvY2F0aW9uIiwiTG9jYXRpb25MaXN0IiwiaW5mbyIsImRpc3BsYXkiLCJmbGV4V3JhcCIsImFsaWduSXRlbXMiLCJqdXN0aWZ5Q29udGVudCIsIm1heEhlaWdodCIsIm92ZXJmbG93IiwidGFyZ2V0cyIsIm1hcCIsInRhcmdldCIsInBsYWNlIl0sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7O0FBQUEsQUFBTzs7Ozs7Ozs7O0FBRVAsSUFBTSxlQUFnQixTQUFoQixBQUFnQixtQkFBWTtRQUFWLEFBQVUsWUFBVixBQUFVLEFBQzlCOzsyQkFDSSxjQUFBLFNBQUssT0FBTyxFQUFDLFNBQUQsQUFBVSxRQUFRLFVBQWxCLEFBQTRCLFFBQVEsWUFBaEQsQUFBWSxBQUFnRDtzQkFBNUQ7d0JBQUEsQUFHUTtBQUhSO0tBQUEsa0JBR1EsY0FBQSxTQUFLLE9BQU8sRUFBQyxTQUFELEFBQVUsUUFBUSxVQUFsQixBQUE0QixRQUFRLGdCQUFwQyxBQUFvRCxnQkFBZ0IsV0FBcEUsQUFBK0UsS0FBSyxVQUFoRyxBQUFZLEFBQThGO3NCQUExRzt3QkFBQSxBQUNLO0FBREw7WUFDSyxBQUFLLFFBQUwsQUFBYSxJQUFJLGtCQUFBOytCQUFVLEFBQUMsb0NBQVMsS0FBSyxPQUFmLEFBQXNCLE9BQU8sVUFBN0IsQUFBdUM7MEJBQXZDOzRCQUFWLEFBQVU7QUFBQTtTQUFBO0FBTDVDLEFBQ0ksQUFHUSxBQUNLLEFBTXBCO0FBWkQsQUFjQTs7a0JBQUEsQUFBZSIsImZpbGUiOiJMb2NhdGlvbkxpc3QuanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL25hdGhhbnF1ZWlqYS9EZXNrdG9wL3Byb2pldG9zL21hcGxpbmstZnJvbnRlbmQtY2hhbGxlbmdlIn0=