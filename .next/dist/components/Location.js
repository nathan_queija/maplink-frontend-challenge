'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _getPrototypeOf = require('babel-runtime/core-js/object/get-prototype-of');

var _getPrototypeOf2 = _interopRequireDefault(_getPrototypeOf);

var _classCallCheck2 = require('babel-runtime/helpers/classCallCheck');

var _classCallCheck3 = _interopRequireDefault(_classCallCheck2);

var _createClass2 = require('babel-runtime/helpers/createClass');

var _createClass3 = _interopRequireDefault(_createClass2);

var _possibleConstructorReturn2 = require('babel-runtime/helpers/possibleConstructorReturn');

var _possibleConstructorReturn3 = _interopRequireDefault(_possibleConstructorReturn2);

var _inherits2 = require('babel-runtime/helpers/inherits');

var _inherits3 = _interopRequireDefault(_inherits2);

var _react = require('react');

var _react2 = _interopRequireDefault(_react);

var _reactRedux = require('react-redux');

var _villain = require('../actions/villain');

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var _jsxFileName = '/Users/nathanqueija/Desktop/projetos/maplink-frontend-challenge/components/Location.js';


var Location = function (_React$Component) {
    (0, _inherits3.default)(Location, _React$Component);

    function Location(props) {
        (0, _classCallCheck3.default)(this, Location);

        var _this = (0, _possibleConstructorReturn3.default)(this, (Location.__proto__ || (0, _getPrototypeOf2.default)(Location)).call(this, props));

        _this.handleClick = _this.handleClick.bind(_this);
        return _this;
    }

    (0, _createClass3.default)(Location, [{
        key: 'handleClick',
        value: function handleClick() {
            this.props.setPossibleLocation(this.props.location);
        }
    }, {
        key: 'render',
        value: function render() {
            var location = this.props.location;

            console.log(this.props);
            return _react2.default.createElement('div', { className: this.props.villain.possibleLocation.location && this.props.villain.possibleLocation.place === this.props.location.place ? 'current-location' : 'location',
                onClick: this.handleClick, __source: {
                    fileName: _jsxFileName,
                    lineNumber: 22
                }
            }, _react2.default.createElement('h3', {
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 25
                }
            }, location.place), _react2.default.createElement('p', {
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 26
                }
            }, _react2.default.createElement('b', {
                __source: {
                    fileName: _jsxFileName,
                    lineNumber: 26
                }
            }, 'Probability:'), ' ', Math.round(location.probability), ' %'));
        }
    }]);

    return Location;
}(_react2.default.Component);

var mapStateToProps = function mapStateToProps(_ref) {
    var villain = _ref.villain;
    return {
        villain: villain
    };
};

exports.default = (0, _reactRedux.connect)(mapStateToProps, { setPossibleLocation: _villain.setPossibleLocation })(Location);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbImNvbXBvbmVudHMvTG9jYXRpb24uanMiXSwibmFtZXMiOlsiUmVhY3QiLCJjb25uZWN0Iiwic2V0UG9zc2libGVMb2NhdGlvbiIsIkxvY2F0aW9uIiwicHJvcHMiLCJoYW5kbGVDbGljayIsImJpbmQiLCJsb2NhdGlvbiIsImNvbnNvbGUiLCJsb2ciLCJ2aWxsYWluIiwicG9zc2libGVMb2NhdGlvbiIsInBsYWNlIiwiTWF0aCIsInJvdW5kIiwicHJvYmFiaWxpdHkiLCJDb21wb25lbnQiLCJtYXBTdGF0ZVRvUHJvcHMiXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBQUEsQUFBTzs7OztBQUNQLEFBQVM7O0FBQ1QsQUFBUTs7Ozs7OztJLEFBR0Y7c0NBRUY7O3NCQUFBLEFBQVksT0FBTTs0Q0FBQTs7OElBQUEsQUFDUixBQUVOOztjQUFBLEFBQUssY0FBYyxNQUFBLEFBQUssWUFBTCxBQUFpQixLQUh0QixBQUdkO2VBQ0g7Ozs7O3NDQUVZLEFBQ1Q7aUJBQUEsQUFBSyxNQUFMLEFBQVcsb0JBQW9CLEtBQUEsQUFBSyxNQUFwQyxBQUEwQyxBQUM3Qzs7OztpQ0FFTztnQkFBQSxBQUNHLFdBQVksS0FEZixBQUNvQixNQURwQixBQUNHLEFBQ1A7O29CQUFBLEFBQVEsSUFBSSxLQUFaLEFBQWlCLEFBQ2pCO21DQUNJLGNBQUEsU0FBSyxXQUNELEtBQUEsQUFBSyxNQUFMLEFBQVcsUUFBWCxBQUFtQixpQkFBbkIsQUFBb0MsWUFBYSxLQUFBLEFBQUssTUFBTCxBQUFXLFFBQVgsQUFBbUIsaUJBQW5CLEFBQW9DLFVBQVUsS0FBQSxBQUFLLE1BQUwsQUFBVyxTQUExRyxBQUFtSCxRQUFuSCxBQUEwSCxxQkFEOUgsQUFDa0osQUFDN0k7eUJBQVMsS0FGZCxBQUVtQjs4QkFGbkI7Z0NBQUEsQUFHSTtBQUhKO2FBQUEsa0JBR0ksY0FBQTs7OEJBQUE7Z0NBQUEsQUFBSztBQUFMO0FBQUEsd0JBSEosQUFHSSxBQUFjLEFBQ2Qsd0JBQUEsY0FBQTs7OEJBQUE7Z0NBQUEsQUFBRztBQUFIO0FBQUEsK0JBQUcsY0FBQTs7OEJBQUE7Z0NBQUE7QUFBQTtBQUFBLGVBQUgsQUFBRyxpQkFBcUIsVUFBQSxBQUFLLE1BQU0sU0FBbkMsQUFBd0IsQUFBb0IsY0FMcEQsQUFDSSxBQUlJLEFBR1g7Ozs7O0VBdkJrQixnQixBQUFNOztBQTBCN0IsSUFBTSxrQkFBa0IsU0FBbEIsQUFBa0Isc0JBQUE7UUFBQSxBQUFHLGVBQUgsQUFBRzs7aUJBQUgsQUFBa0I7QUFBQSxBQUN0QztBQURKLEFBS0E7O2tCQUFlLHlCQUFBLEFBQVEsaUJBQWlCLEVBQXpCLEFBQXlCLEFBQUMscURBQXpDLEFBQWUsQUFBZ0QiLCJmaWxlIjoiTG9jYXRpb24uanMiLCJzb3VyY2VSb290IjoiL1VzZXJzL25hdGhhbnF1ZWlqYS9EZXNrdG9wL3Byb2pldG9zL21hcGxpbmstZnJvbnRlbmQtY2hhbGxlbmdlIn0=