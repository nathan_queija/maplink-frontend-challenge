import React, {Component} from 'react'
import RaisedButton from 'material-ui/RaisedButton'
import Dialog from 'material-ui/Dialog'
import {deepOrange500} from 'material-ui/styles/colors'
import FlatButton from 'material-ui/FlatButton'
import getMuiTheme from 'material-ui/styles/getMuiTheme'
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import injectTapEventPlugin from 'react-tap-event-plugin'
import Map from 'components/Map';
import withRedux from 'next-redux-wrapper'
import initStore from 'utils/store';
import { setVillainLocation} from 'actions/villain';
import Paper from 'material-ui/Paper';
import LocationList from 'components/LocationList';

import Layout from '../components/Layout';



const styles = {
  container: {
    textAlign: 'center',
    paddingTop: 0
  }
}

const muiTheme = {
  palette: {
    accent1Color: deepOrange500
  }
}

class Index extends Component {

  constructor (props, context) {
    super(props, context);


  }

  handleRequestClose = () => {
    this.setState({
      open: false
    })
  }

  getContent(){
    if(this.props.villain.info.targets){
      return (
            <div style={{display: 'flex', flexDirection: 'column', flexWrap: 'wrap', height: 'auto'}}>
              <LocationList  info={this.props.villain.info}/>
              <Map villainPosition={this.props.villain.villainPosition}/>
            </div>
          );
    }

    return <h2>Hey, Bat! We're listening. Just send us the villain coordinates.</h2>
  }

  render () {

    return (
      <Layout>
        <div style={styles.container}>
          <Paper>
          {this.getContent()}
          </Paper>

        </div>
      </Layout>
    )
  }
}

const mapStateToProps = ({ villain }) => ({
  villain
});


export default withRedux(initStore, mapStateToProps, {
  setVillainLocation
})(Index);